db.fruits.insertMany(
    [
        {
            name:"Apple",
            supplier:"Red Farms Inc",
            stocks:20,
            price:40,
            onSale:true,
        },
        {
            name:"Banana",
            supplier:"Yellow Farms",
            stocks:15,
            price:20,
            onSale:true,
        },
        {
            name:"Kiwi",
            supplier:"Green Farming and Canning",
            stocks:25,
            price:50,
            onSale:true,
        },
        {
            name:"Mango",
            supplier:"Yellow Farms",
            stocks:10,
            price:60,
            onSale:true,
        },
        {
            name:"Dragon Fruit",
            supplier:"Red Farms Inc",
            stocks:10,
            price:60,
            onSale:true,
        },
    ]
)
// Aggregation Pipeline Stages
    // sum
        db.fruits.aggregate([
            {$match: {onSale:true}},
            {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
        ])

        db.fruits.aggregate([
            {$match: {onSale:true}},
            {$group: {_id:null,totalStocks:{$sum:"$stocks"}}}
        ])


        db.fruits.aggregate([
            {$match: {onSale:false}},
            {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
        ])
    // avg
        db.fruits.aggregate([
            {$match: {onSale:true}},
            {$group: {_id:"$supplier",avgStock:{$avg:"$stocks"}}}
        ])
    // max
        db.fruits.aggregate([
            {$match: {onSale:true}},
            {$group: {_id:"highestStockOnSale",maxStock:{$max:"$stocks"}}}
        ])
    // min
        db.fruits.aggregate([
            {$match: {onSale:true}},
            {$group: {_id:"highestStockOnSale",min:{$min:"$stocks"}}}
        ])
        
// count
    db.fruits.aggregate([
        {$match: {onSale:true}},
        {$count: "itemsOnSale"}
    ])
// out : save / output the results in a new collection
// overwrite the collection if it already exist  
    db.fruits.aggregate([
        {$match: {onSale:true}},
        {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}},
        {$out: "stocksPerSupplier"}
    ])